<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class District extends Model
{
    
    public $table = "districts";
    
    public $primaryKey = "id";
    
    public $timestamps = true;
    
    public $fillable = ["code", "name", "city_id"];
    
    public $hidden = ["deleted_at"];
    
    public $rules = ["code" => "required|unique:districts", "name" => "required"];
}
