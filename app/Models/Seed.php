<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Seed extends Model
{
    public $table = 'seeds';
    
    public $primaryKey = 'id';
    
    public $timestamps = true;
    
    public $fillable = ['purpose', 'address', 'type', 'quantity', 'user_id', 'plant_id', 'lon', 'lat', 'location_id'];
    
    public $hidden = ['deleted_at'];

    public $rules = [
        'purpose' => 'required',
        'address' => 'required',
        'type' => 'required',
        'quantity' => 'integer|required',
        'user_id' => 'required|exists:users,id',        
        'lon' => 'required',
        'lat' => 'required',
        'location_id' => 'required|exists:locations,id',
    ];

    public function user() {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }

    public function plant() {
        return $this->belongsTo('App\Models\Plant', 'plant_id', 'id');
    }

    public function scopeCreator($query, $id) {
        return $query->where('user_id', $id);
    }

    public function location() {
        return $this->belongsTo('App\Models\Location', 'location_id', 'id');
    }
}
