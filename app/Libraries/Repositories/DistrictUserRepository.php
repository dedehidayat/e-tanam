<?php
namespace App\Libraries\Repositories;

use App\Libraries\Repositories\Common\CRUDRepository;
// Ganti Class Modelnya nya  saja
use App\Models\DistrictUser as Model;

class DistrictUserRepository extends CRUDRepository {
    
    public function model() {
        return Model::class;
    }
    
}
