<?php
namespace App\Http\Controllers\API;

use App\Http\Requests;
use Mitul\Controller\AppBaseController;
use Mitul\Generator\Utils\ResponseManager;
use Illuminate\Http\Request;
use Response;


use App\Libraries\Repositories\PlantingRepository;
use App\Libraries\Repositories\SeedbedRepository;

class DashboardAPIController extends AppBaseController
{
    
    function __construct(PlantingRepository $plantings, SeedbedRepository $seedbeds) {
    	$this->plantings = $plantings;
    	$this->seedbeds = $seedbeds;

        $this->middleware('oauth_permission');
        $this->beforeFilter('oauth', ['except' => ['plantings', 'seedbeds', 'statistics', 'summary']]);
    }
    
    protected function response($data, $meta, $code = 200) {
        return Response::json(ResponseManager::makeResult($data, $meta), $code, [], JSON_NUMERIC_CHECK);
    }

	public function plantings(Request $request) {

        $input = $request->all();
        
        $result = $this->plantings->search($input);
        
        $records = $result[0];
        
        $meta = array('total' => $result['total'], 'count' => count($records), 'offset' => isset($input['offset']) ? (int)$input['offset'] : 0, 'last_updated' => $this->plantings->lastUpdated(), 'status' => 'Records retrieved successfully.', 'error' => 'Success');
        
        return $this->response($records->toArray(), $meta);
	}

	public function seedbeds(Request $request) {

        $input = $request->all();
        
        $result = $this->seedbeds->search($input);
        
        $records = $result[0];
        
        $meta = array('total' => $result['total'], 'count' => count($records), 'offset' => isset($input['offset']) ? (int)$input['offset'] : 0, 'last_updated' => $this->seedbeds->lastUpdated(), 'status' => 'Records retrieved successfully.', 'error' => 'Success');
        
        return $this->response($records->toArray(), $meta);
	}

    public function statistics(Request $request) {
        $input = $request->all();
        
        $result = $this->plantings->search($input, ['emission']);
        
        $startDate = $input['start_date'];
        $endDate = $input['end_date'];

        $records = \DB::select("select b.name as label, count(a.id) as total, sum(a.quantity) as value 
        from plantings a
        join emissions b on a.emission_id = b.id  
        where planting_date between '$startDate' and '$endDate'
        group by a.emission_id");

        $meta = array('total' => $result['total'], 'count' => count($records), 'offset' => isset($input['offset']) ? (int)$input['offset'] : 0, 'last_updated' => $this->plantings->lastUpdated(), 'status' => 'Records retrieved successfully.', 'error' => 'Success');
        
        return $this->response($records, $meta);
    }

    public function summary(Request $request) {
        $input = $request->all();
        
        $result = $this->plantings->search($input);

        $now = date('Y');
        $startDate = $input['start_date'];
        $endDate = $input['end_date'];
		$grafik = $input['grafik'];
		
		$str="";
		
		if ($grafik==1){
		  $str = "
(select 'Total Penanaman' as label, count(a.id) as total, COALESCE(sum(a.quantity), 0) as value, 'Pohon' as unit, planting_date as 'date' from plantings a where planting_date between '$startDate' and '$endDate')
";	
		}else if ($grafik==2){
			$str = "(select 
'Capaian Penurunan Emisi' as label, 
count(a.id) as total, 
COALESCE(sum(($now - a.year) * b.emission * a.quantity * a.grow_percent / 100 ), 0) as value, 
'Ton Co<sup>2</sup> Eq' as unit, 
planting_date as 'date' 
from 
plantings a 
join emissions b on b.id = a.emission_id
where planting_date between '$startDate' and '$endDate')";
		}else if ($grafik==3){
			$str=" (select 'Jumlah Stok Bibit' as label, count(a.id) as total, COALESCE(sum(a.quantity), 0) as value, '' as unit, planting_date as 'date' from plantings a where planting_date between '$startDate' and '$endDate') ";
		}else if ($grafik==4){
			$str="
			select 'Persemaian' as label, count(a.id) as total, COALESCE(count(a.id), 0) as value, '' as unit,
created_at as 'date' from seedbeds a 
where created_at between '$startDate' and '$endDate'
			";
		}		
        
        $records = \DB::select($str);

        $meta = array('total' => $result['total'], 'count' => count($records), 'offset' => isset($input['offset']) ? (int)$input['offset'] : 0, 'last_updated' => $this->plantings->lastUpdated(), 'status' => 'Records retrieved successfully.', 'error' => 'Success');
		
		/*
		union

(select 
'Capaian Penurunan Emisi' as label, 
count(a.id) as total, 
COALESCE(sum(($now - a.year) * b.emission * a.quantity * a.grow_percent / 100 ), 0) as value, 
'Ton Co<sup>2</sup> Eq' as unit, 
planting_date as 'date' 
from 
plantings a 
join emissions b on b.id = a.emission_id
where planting_date between '$startDate' and '$endDate')

union

(select 'Jumlah Stok Bibit' as label, count(a.id) as total, COALESCE(sum(a.quantity), 0) as value, '' as unit, planting_date as 'date' from plantings a where planting_date between '$startDate' and '$endDate')
		
		*/
        
        return $this->response($records, $meta);
    }
	
	
	

}
