<?php
namespace App\Http\Controllers\API;

use App\Http\Controllers\API\Common\CRUDAPIController;
use Illuminate\Container\Container as Application;
// Ganti Class Repository nya  saja
use App\Libraries\Repositories\ApprovalSeedRepository as Repository;

class ApprovalSeedAPIController extends CRUDAPIController
{
    
    function __construct(Application $app, Repository $repo) {
        $this->repo = $repo;
        parent::__construct($app);
    }

}
