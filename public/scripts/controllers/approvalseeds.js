'use strict';
/**
 * @ngdoc function
 * @name minovateApp.controller:ApprovalSeedsCtrl
 * @description
 * # ApprovalSeedsCtrl
 * Controller of the minovateApp
 */
angular.module('minovateApp').config(['$stateProvider', function($stateProvider) {
    $stateProvider.state('app.approvalseeds', {
        url: '/persetujuan',
        abstract: true,
        controller: 'ApprovalSeedsCtrl',
        templateUrl: 'views/approvalseeds/main.html'
    }).state('app.approvalseeds.index', {
        url: '/index?page',
        views: {
            'page-content': {
                templateUrl: 'views/approvalseeds/index.html',
                controller: 'ApprovalSeedsIndexCtrl'
            }
        }
    }).state('app.approvalseeds.add', {
        url: '/tambah?backTo=null',
        views: {
            'page-content': {
                templateUrl: 'views/approvalseeds/form.html',
                controller: 'ApprovalSeedsAddCtrl'
            }
        },
        resolve: {
            seeds: ['storeSeeds', '$q', 'appAuth', function(storeSeeds, $q, appAuth) {
                var defer = $q.defer();
                storeSeeds.query({
                    access_token: appAuth.token,
                    'with': '["user"]'
                }).$promise.then(function(response) {
                    defer.resolve(response.data || []);
                }, function(response) {
                    defer.reject(response);
                });
                return defer.promise;
            }],
            seedbeds: ['storeSeedbeds', '$q', 'appAuth', function(storeSeedbeds, $q, appAuth) {
                var defer = $q.defer();
                storeSeedbeds.query({
                    access_token: appAuth.token
                }).$promise.then(function(response) {
                    defer.resolve(response.data || []);
                }, function(response) {
                    defer.reject(response);
                });
                return defer.promise;
            }],
            approvalseedsDetail: [function() {
                return null;
            }]
        },
        data: {
            accessLevel: AUTHConfig.accessLevels.user
        }
    }).state('app.approvalseeds.detail', {
        url: '/detail/:id',
        views: {
            'page-content': {
                templateUrl: 'views/approvalseeds/detail.html',
                controller: 'ApprovalSeedsDetailCtrl'
            }
        },
        resolve: {
            approvalseedsDetail: ['$stateParams', '$q', 'storeApprovalSeeds', 'appAuth', function($stateParams, $q, storeApprovalSeeds, appAuth) {
                if ($stateParams.id) {
                    var defer = $q.defer();
                    storeApprovalSeeds.get({
                        id: $stateParams.id,
                        access_token: appAuth.token
                    }, function(a) {
                        defer.resolve(a);
                    });
                    return defer.promise;
                } else {
                    return null;
                }
            }]
        },
        data: {
            accessLevel: AUTHConfig.accessLevels.user
        }
    });
}]).factory('storeApprovalSeeds', ['$resource', 'appConfig', function($resource, appConfig) {
    return $resource(appConfig.api.get('baseUrl') + '/approvalseeds/:id', {
        id: '@id'
    }, {
        get: {
            method: 'GET',
            cache: false
        },
        query: {
            method: 'GET',
            cache: false,
            isArray: false
        },
        update: {
            method: 'PUT'
        }
    });
}]).controller('ApprovalSeedsCtrl', ['$scope', function($scope) {
    $scope.pageInformation = {
        title: 'Persetujuan Bibit',
        route: 'approvalseeds',
        subtitle: ''
    };
}]).controller('ApprovalSeedsIndexCtrl', ['$scope', 'storeApprovalSeeds', '$state', '$stateParams', 'appConfig', 'appAuth', 'appPopup', '$filter', '$http', function($scope, storeApprovalSeeds, $state, $stateParams, appConfig, appAuth, appPopup, $filter, $http) {
    $scope.limit = 15, $scope.page = $stateParams.page || 1;
    $scope.data = [], $scope.message = {};
    $scope.getOffset = function() {
        return ($scope.page - 1) * $scope.limit;
    }
    $scope.paging = function() {
        $state.transitionTo('app.' + $scope.pageInformation.route + '.index', {
            page: $scope.page
        });
    };
    $scope.query = '';
    $scope.getData = function(tableState) {
        angular.element('.tile').addClass('refreshing');
        var params = {
            access_token: appAuth.token,
            limit: $scope.limit,
            offset: $scope.getOffset(),
            'with': '["seed.user", "seedbedDetail"]'
        };
        // Filtering
        if ($scope.query) params.filter = JSON.stringify([{
            "field": "name",
            "operator": "like",
            "value": $scope.query
        }]);
        // Sorting
        if (tableState && tableState.sort.predicate) {
            params.order = JSON.stringify([{
                field: tableState.sort.predicate,
                sort: tableState.sort.reverse ? 'desc' : 'asc',
            }]);
        }
        storeApprovalSeeds.query(params).$promise.then(function(response, status) {
            if (response.data) {
                $scope.data = response.data;
                $scope.message = response.meta;
            }
            angular.element('.tile.refreshing').removeClass('refreshing');
        }, function(response, status) {
            angular.element('.tile.refreshing').removeClass('refreshing');
            appPopup.showResponseAPI(response, status);
        });
    };
    $scope.selectAll = function(status) {
        angular.forEach($scope.data, function(value, key) {
            value.selected = !!status;
        });
    }
    $scope.deleteRecord = function(item) {
        angular.element('.tile').addClass('refreshing');
        // DELETE MENGGUNAKAN $http, karena $resource tidak mau mengirim body
        var config = {
            method: 'DELETE',
            url: appConfig.api.get('baseUrl') + '/approvalseeds/' + item.id,
            params: {
                access_token: appAuth.token
            },
            data: {
                access_token: appAuth.token
            }
        };
        return $http(config).success(function(response, status) {
            angular.element('.tile.refreshing').removeClass('refreshing');
            $scope.getData();
            return appPopup.showResponseAPI(response, status);
        }).error(function(response, status) {
            angular.element('.tile.refreshing').removeClass('refreshing');
            return appPopup.showResponseAPI(response, status);
        });
    }
    $scope.bulkActions = appAuth.authenticated ? [{
        name: 'Delete All',
        fn: function() {
            var data = $filter('filter')($scope.data, function(value, key) {
                return value.selected === true;
            });
            if (data.length) {
                var deleteRecrusive = function (items) {
                    if (!items || !items.length) return;
                    var item = items[0];
                    $scope.deleteRecord(item).success(function(response, status) {
                        if (status === 200) {
                            items.splice(0, 1); // remove item yang sudah dihapus
                            deleteRecrusive(items);
                        }
                    });
                }
                return deleteRecrusive(data);
            }
            return appPopup.toast('No item selected', 'Warning', {
                iconType: 'fa-warning',
                iconClass: 'bg-warning'
            });
        }
    }] : null;
    $scope.$watch('query', function(newVal, oldVal) {
        if (!angular.equals(newVal, oldVal)) {
            $scope.getData();
        }
    });
    $scope.getData();
}]).controller('ApprovalSeedsAddCtrl', ['$scope', 'appConfig', '$q', 'appAuth', '$state', '$stateParams', 'storeApprovalSeeds', 'approvalseedsDetail', 'seeds', 'seedbeds', 'appPopup', 'storeSeedbeds', function($scope, appConfig, $q, appAuth, $state, $stateParams, storeApprovalSeeds, approvalseedsDetail, seeds, seedbeds, appPopup, storeSeedbeds) {
    var isContinueAdd = false;
    $scope.data = approvalseedsDetail && approvalseedsDetail.data ? approvalseedsDetail.data : {};
    $scope.save = function(isContinue) {
        isContinueAdd = !!isContinue;
        if (!$scope.data.id) {
            $scope.promise = storeApprovalSeeds.save(angular.extend({
                access_token: appAuth.token,
                seed_id: $scope.seed.id,
                seedbed_detail_id: $scope.seedbedDetail.id                
            }, $scope.data), function(response, status) {
                if (isContinueAdd) {
                    $scope.data = {};
                    $scope.form.$setPristine();
                } else {
                    if ($state.params.backTo) {
                        $state.go($state.params.backTo)
                    } else {
                        $state.go('app.' + $scope.pageInformation.route + '.index')
                    }
                }
                return appPopup.showResponseAPI(response, status);
            }, function(response, status) {
                return appPopup.showResponseAPI(response, status);
            });
        } else {
            $scope.promise = storeApprovalSeeds.update({
                id: $scope.data.id
            }, angular.extend({
                access_token: appAuth.token,
                seed_id: $scope.seed.id,
                seedbed_detail_id: $scope.seedbedDetail.id                
            }, $scope.data), function(response, status) {
                $state.go('app.' + $scope.pageInformation.route + '.index');
                return appPopup.showResponseAPI(response, status);
            }, function(response, status) {
                return appPopup.showResponseAPI(response, status);
            });
        }
    };


    $scope.seeds = seeds;
    $scope.seedbeds = seedbeds;
    $scope.seedbedDetails = [];


    $scope.onSeedbedChanged = function(seedbed) {
        $scope.promise = storeSeedbeds.get({
            id: seedbed.id,
            access_token: appAuth.token
        }, function(response) {
            $scope.seedbedDetails = response.data.details
        });
    }
}])
.controller('ApprovalSeedsDetailCtrl', ['$scope', '$state', 'approvalseedsDetail', function($scope, $state, approvalseedsDetail){
    // $scope.seedbed = approvalseedsDetail.data[$state.params.id][0].seedbed;
    $scope.data = approvalseedsDetail.data;
}]);;