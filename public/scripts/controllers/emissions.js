'use strict';
/**
 * @ngdoc function
 * @name minovateApp.controller:EmissionsCtrl
 * @description
 * # EmissionsCtrl
 * Controller of the minovateApp
 */
angular.module('minovateApp').config(['$stateProvider', function($stateProvider) {
    $stateProvider.state('app.emissions', {
        url: '/faktor-emisi',
        abstract: true,
        controller: 'EmissionsCtrl',
        templateUrl: 'views/emissions/main.html'
    }).state('app.emissions.index', {
        url: '/index?page',
        views: {
            'page-content': {
                templateUrl: 'views/emissions/index.html',
                controller: 'EmissionsIndexCtrl'
            }
        }
    }).state('app.emissions.add', {
        url: '/tambah?backTo=null',
        views: {
            'page-content': {
                templateUrl: 'views/emissions/form.html',
                controller: 'EmissionsAddCtrl'
            }
        },
        resolve: {
            emissionsDetail: [function() {
                return null;
            }]
        },
        data: {
            accessLevel: AUTHConfig.accessLevels.user
        }
    }).state('app.emissions.update', {
        url: '/perbarui/:id',
        views: {
            'page-content': {
                templateUrl: 'views/emissions/form.html',
                controller: 'EmissionsAddCtrl'
            }
        },
        resolve: {
            emissionsDetail: ['$stateParams', '$q', 'storeEmissions', 'appAuth', function($stateParams, $q, storeEmissions, appAuth) {
                if ($stateParams.id) {
                    var defer = $q.defer();
                    storeEmissions.get({
                        id: $stateParams.id,
                        access_token: appAuth.token
                    }, function(a) {
                        defer.resolve(a);
                    });
                    return defer.promise;
                } else {
                    return null;
                }
            }]
        },
        data: {
            accessLevel: AUTHConfig.accessLevels.user
        }
    });
}]).factory('storeEmissions', ['$resource', 'appConfig', function($resource, appConfig) {
    return $resource(appConfig.api.get('baseUrl') + '/emissions/:id', {
        id: '@id'
    }, {
        get: {
            method: 'GET',
            cache: false
        },
        query: {
            method: 'GET',
            cache: false,
            isArray: false
        },
        update: {
            method: 'PUT'
        }
    });
}]).controller('EmissionsCtrl', ['$scope', function($scope) {
    $scope.pageInformation = {
        title: 'Faktor Emisi',
        route: 'emissions',
        subtitle: ''
    };
}]).controller('EmissionsIndexCtrl', ['$scope', 'storeEmissions', '$state', '$stateParams', 'appConfig', 'appAuth', 'appPopup', '$filter', '$http', function($scope, storeEmissions, $state, $stateParams, appConfig, appAuth, appPopup, $filter, $http) {
    $scope.limit = 15, $scope.page = $stateParams.page || 1;
    $scope.data = [], $scope.message = {};
    $scope.getOffset = function() {
        return ($scope.page - 1) * $scope.limit;
    }
    $scope.paging = function() {
        $state.transitionTo('app.' + $scope.pageInformation.route + '.index', {
            page: $scope.page
        });
    };
    $scope.query = '';
    $scope.getData = function(tableState) {
        angular.element('.tile').addClass('refreshing');
        var params = {
            access_token: appAuth.token,
            limit: $scope.limit,
            offset: $scope.getOffset()
        };
        // Filtering
        if ($scope.query) params.filter = JSON.stringify([{
            "field": "name",
            "operator": "like",
            "value": $scope.query
        }]);
        // Sorting
        if (tableState && tableState.sort.predicate) {
            params.order = JSON.stringify([{
                field: tableState.sort.predicate,
                sort: tableState.sort.reverse ? 'desc' : 'asc',
            }]);
        }
        storeEmissions.query(params).$promise.then(function(response, status) {
            if (response.data) {
                $scope.data = response.data;
                $scope.message = response.meta;
            }
            angular.element('.tile.refreshing').removeClass('refreshing');
        }, function(response, status) {
            angular.element('.tile.refreshing').removeClass('refreshing');
            appPopup.showResponseAPI(response, status);
        });
    };
    $scope.selectAll = function(status) {
        angular.forEach($scope.data, function(value, key) {
            value.selected = !!status;
        });
    }
    $scope.deleteRecord = function(item) {
        angular.element('.tile').addClass('refreshing');
        // DELETE MENGGUNAKAN $http, karena $resource tidak mau mengirim body
        var config = {
            method: 'DELETE',
            url: appConfig.api.get('baseUrl') + '/emissions/' + item.id,
            params: {
                access_token: appAuth.token
            },
            data: {
                access_token: appAuth.token
            }
        };
        return $http(config).success(function(response, status) {
            angular.element('.tile.refreshing').removeClass('refreshing');
            $scope.getData();
            return appPopup.showResponseAPI(response, status);
        }).error(function(response, status) {
            angular.element('.tile.refreshing').removeClass('refreshing');
            return appPopup.showResponseAPI(response, status);
        });
    }
    $scope.bulkActions = appAuth.authenticated ? [{
        name: 'Delete All',
        fn: function() {
            var data = $filter('filter')($scope.data, function(value, key) {
                return value.selected === true;
            });
            if (data.length) {
                var deleteRecrusive = function (items) {
                    if (!items || !items.length) return;
                    var item = items[0];
                    $scope.deleteRecord(item).success(function(response, status) {
                        if (status === 200) {
                            items.splice(0, 1); // remove item yang sudah dihapus
                            deleteRecrusive(items);
                        }
                    });
                }
                return deleteRecrusive(data);
            }
            return appPopup.toast('No item selected', 'Warning', {
                iconType: 'fa-warning',
                iconClass: 'bg-warning'
            });
        }
    }] : null;
    $scope.$watch('query', function(newVal, oldVal) {
        if (!angular.equals(newVal, oldVal)) {
            $scope.getData();
        }
    });
    $scope.getData();
}]).controller('EmissionsAddCtrl', ['$scope', 'appConfig', '$q', 'appAuth', '$state', '$stateParams', 'storeEmissions', 'emissionsDetail', 'appPopup', function($scope, appConfig, $q, appAuth, $state, $stateParams, storeEmissions, emissionsDetail, appPopup) {
    var isContinueAdd = false;
    $scope.data = emissionsDetail && emissionsDetail.data ? emissionsDetail.data : {};
    $scope.save = function(isContinue) {
        isContinueAdd = !!isContinue;
        if (!$scope.data.id) {
            $scope.promise = storeEmissions.save(angular.extend({
                access_token: appAuth.token              
            }, $scope.data), function(response, status) {
                if (isContinueAdd) {
                    $scope.data = {};
                    $scope.form.$setPristine();
                } else {
                    if ($state.params.backTo) {
                        $state.go($state.params.backTo)
                    } else {
                        $state.go('app.' + $scope.pageInformation.route + '.index')
                    }
                }
                return appPopup.showResponseAPI(response, status);
            }, function(response, status) {
                return appPopup.showResponseAPI(response, status);
            });
        } else {
            $scope.promise = storeEmissions.update({
                id: $scope.data.id
            }, angular.extend({
                access_token: appAuth.token              
            }, $scope.data), function(response, status) {
                $state.go('app.' + $scope.pageInformation.route + '.index');
                return appPopup.showResponseAPI(response, status);
            }, function(response, status) {
                return appPopup.showResponseAPI(response, status);
            });
        }
    };

    $scope.emissionTypes = ['Fast Growing', 'Slow Growing', 'Non Fast dan Slow Growing'];
}]);