'use strict';
/**
 * @ngdoc function
 * @name minovateApp.controller:PlantingsCtrl
 * @description
 * # PlantingsCtrl
 * Controller of the minovateApp
 */
angular.module('minovateApp').config(['$stateProvider', function($stateProvider) {
    $stateProvider.state('app.plantings', {
        url: '/penanaman',
        abstract: true,
        controller: 'PlantingsCtrl',
        templateUrl: 'views/plantings/main.html'
    }).state('app.plantings.index', {
        url: '/index?page',
        views: {
            'page-content': {
                templateUrl: 'views/plantings/index.html',
                controller: 'PlantingsIndexCtrl'
            }
        }
    }).state('app.plantings.add', {
        url: '/tambah?backTo=null',
        views: {
            'page-content': {
                templateUrl: 'views/plantings/form.html',
                controller: 'PlantingsAddCtrl'
            }
        },
        resolve: {
            users: ['storeUsers', '$q', 'appAuth', function(storeUsers, $q, appAuth) {
                var defer = $q.defer();
                storeUsers.query({
                    access_token: appAuth.token
                }).$promise.then(function(response) {
                    defer.resolve(response.data || []);
                }, function(response) {
                    defer.reject(response);
                });
                return defer.promise;
            }],
            emissions: ['storeEmissions', '$q', 'appAuth', function(storeEmissions, $q, appAuth) {
                var defer = $q.defer();
                storeEmissions.query({
                    access_token: appAuth.token
                }).$promise.then(function(response) {
                    defer.resolve(response.data || []);
                }, function(response) {
                    defer.reject(response);
                });
                return defer.promise;
            }],
            plantingsDetail: [function() {
                return null;
            }]
        },
        data: {
            accessLevel: AUTHConfig.accessLevels.user
        }
    }).state('app.plantings.update', {
        url: '/perbarui/:id',
        views: {
            'page-content': {
                templateUrl: 'views/plantings/form.html',
                controller: 'PlantingsAddCtrl'
            }
        },
        resolve: {
            users: ['storeUsers', '$q', 'appAuth', function(storeUsers, $q, appAuth) {
                var defer = $q.defer();
                storeUsers.query({
                    access_token: appAuth.token
                }).$promise.then(function(response) {
                    defer.resolve(response.data || []);
                }, function(response) {
                    defer.reject(response);
                });
                return defer.promise;
            }],
            emissions: ['storeEmissions', '$q', 'appAuth', function(storeEmissions, $q, appAuth) {
                var defer = $q.defer();
                storeEmissions.query({
                    access_token: appAuth.token
                }).$promise.then(function(response) {
                    defer.resolve(response.data || []);
                }, function(response) {
                    defer.reject(response);
                });
                return defer.promise;
            }],
            plantingsDetail: ['$stateParams', '$q', 'storePlantings', 'appAuth', function($stateParams, $q, storePlantings, appAuth) {
                if ($stateParams.id) {
                    var defer = $q.defer();
                    storePlantings.get({
                        id: $stateParams.id,
                        access_token: appAuth.token
                    }, function(a) {
                        defer.resolve(a);
                    });
                    return defer.promise;
                } else {
                    return null;
                }
            }]
        },
        data: {
            accessLevel: AUTHConfig.accessLevels.user
        }
    });
}]).factory('storePlantings', ['$resource', 'appConfig', function($resource, appConfig) {
    return $resource(appConfig.api.get('baseUrl') + '/plantings/:id', {
        id: '@id'
    }, {
        get: {
            method: 'GET',
            cache: false
        },
        query: {
            method: 'GET',
            cache: false,
            isArray: false
        },
        update: {
            method: 'PUT'
        },
        statistic: {
            method: 'GET',
            url: appConfig.api.get('baseUrl') + '/plantings/statistic'
        }
    });
}]).controller('PlantingsCtrl', ['$scope', function($scope) {
    $scope.pageInformation = {
        title: 'Penanaman',
        route: 'plantings',
        subtitle: ''
    };
}]).controller('PlantingsIndexCtrl', ['$scope', 'storePlantings', '$state', '$stateParams', 'appConfig', 'appAuth', 'appPopup', '$filter', '$http', function($scope, storePlantings, $state, $stateParams, appConfig, appAuth, appPopup, $filter, $http) {
    $scope.limit = 15, $scope.page = $stateParams.page || 1;
    $scope.data = [], $scope.message = {};
    $scope.getOffset = function() {
        return ($scope.page - 1) * $scope.limit;
    }
    $scope.paging = function() {
        $state.transitionTo('app.' + $scope.pageInformation.route + '.index', {
            page: $scope.page
        });
    };
    $scope.query = '';
    $scope.getData = function(tableState) {
        angular.element('.tile').addClass('refreshing');
        var params = {
            access_token: appAuth.token,
            limit: $scope.limit,
            offset: $scope.getOffset(),
            'with': '["user", "emission"]'
        };
        // Filtering
        var filter = [];
        if ($scope.query) {
            filter.push({
                field: 'name',
                operator: 'like',
                value: $scope.query
            });
        }
        // Selain admin hanya bisa lihat punya sendiri
        if ([3, 4, 5].indexOf(appAuth.profile.roles[0].id) !== -1) {
            filter.push({
                field: 'user_id',
                operator: '=',
                value: appAuth.profile.id
            });
        }
        if (filter.length) params.filter = JSON.stringify(filter);
        // Sorting
        if (tableState && tableState.sort.predicate) {
            params.order = JSON.stringify([{
                field: tableState.sort.predicate,
                sort: tableState.sort.reverse ? 'desc' : 'asc',
            }]);
        }
        storePlantings.query(params).$promise.then(function(response, status) {
            if (response.data) {
                $scope.data = response.data;
                $scope.message = response.meta;
            }
            angular.element('.tile.refreshing').removeClass('refreshing');
        }, function(response, status) {
            angular.element('.tile.refreshing').removeClass('refreshing');
            appPopup.showResponseAPI(response, status);
        });
    };
    $scope.selectAll = function(status) {
        angular.forEach($scope.data, function(value, key) {
            value.selected = !!status;
        });
    }
    $scope.deleteRecord = function(item) {
        angular.element('.tile').addClass('refreshing');
        // DELETE MENGGUNAKAN $http, karena $resource tidak mau mengirim body
        var config = {
            method: 'DELETE',
            url: appConfig.api.get('baseUrl') + '/plantings/' + item.id,
            params: {
                access_token: appAuth.token
            },
            data: {
                access_token: appAuth.token
            }
        };
        return $http(config).success(function(response, status) {
            angular.element('.tile.refreshing').removeClass('refreshing');
            $scope.getData();
            return appPopup.showResponseAPI(response, status);
        }).error(function(response, status) {
            angular.element('.tile.refreshing').removeClass('refreshing');
            return appPopup.showResponseAPI(response, status);
        });
    }
    $scope.bulkActions = appAuth.authenticated ? [{
        name: 'Delete All',
        fn: function() {
            var data = $filter('filter')($scope.data, function(value, key) {
                return value.selected === true;
            });
            if (data.length) {
                var deleteRecrusive = function (items) {
                    if (!items || !items.length) return;
                    var item = items[0];
                    $scope.deleteRecord(item).success(function(response, status) {
                        if (status === 200) {
                            items.splice(0, 1); // remove item yang sudah dihapus
                            deleteRecrusive(items);
                        }
                    });
                }
                return deleteRecrusive(data);
            }
            return appPopup.toast('No item selected', 'Warning', {
                iconType: 'fa-warning',
                iconClass: 'bg-warning'
            });
        }
    }] : null;
    $scope.$watch('query', function(newVal, oldVal) {
        if (!angular.equals(newVal, oldVal)) {
            $scope.getData();
        }
    });
    $scope.getData();
}]).controller('PlantingsAddCtrl', ['$scope', 'appConfig', '$q', 'appAuth', '$state', '$stateParams', 'storePlantings', 'users', 'plantingsDetail', 'appPopup', 'Upload', 'storeLocations', 'emissions', function($scope, appConfig, $q, appAuth, $state, $stateParams, storePlantings, users, plantingsDetail, appPopup, Upload, storeLocations, emissions) {
    var isContinueAdd = false;
    $scope.data = plantingsDetail && plantingsDetail.data ? plantingsDetail.data : {
        planting_date: new Date(),
        user_id: appAuth.profile.id
    };
    $scope.years = [];
    var now = (new Date()).getFullYear(), $i = 0;
    do {
        $scope.years.push(now - $i++);
    } while ($i <= 5);
    $scope.save = function(isContinue) {
        isContinueAdd = !!isContinue;
        if ($scope.files.length) {
            return doUploadAll();
        }
        if (!$scope.data.id) {
            $scope.promise = storePlantings.save(angular.extend($scope.data, {
                access_token: appAuth.token,
                lon: $scope.coord.lon, //coord.lon
                lat: $scope.coord.lat,
                location_id: $scope.location ? $scope.location.id : $scope.village.id,
                emission_id: $scope.emission.id,
                type: $scope.emission.type == 'Non Fast dan Slow Growing' ? $scope.type : ''
				//sumber_dana: $scope.sumberdana
				
            }), function(response, status) {
                if (isContinueAdd) {
                    $scope.data = {};
                    $scope.coord = {};
                    $scope.form.$setPristine();
                } else {
                    if ($state.params.backTo) {
                        $state.go($state.params.backTo)
                    } else {
                        $state.go('app.' + $scope.pageInformation.route + '.index')
                    }
                }
                return appPopup.showResponseAPI(response, status);
            }, function(response, status) {
                return appPopup.showResponseAPI(response, status);
            });
        } else {
            $scope.promise = storePlantings.update({
                id: $scope.data.id
            }, angular.extend($scope.data, {
                access_token: appAuth.token,
                lon: $scope.coord.lon,
                lat: $scope.coord.lat,
                location_id: $scope.location ? $scope.location.id : $scope.village.id,
                emission_id: $scope.emission.id,
                type: $scope.emission.type == 'Non Fast dan Slow Growing' ? $scope.type : '',				
                oldPhotos: $scope.oldPhotos.map(function(item){
                    return {
                        id: item.id,
                        caption: item.caption,
                        deleted: item.deleted
                    }
                })
            }), function(response, status) {
                $state.go('app.' + $scope.pageInformation.route + '.index');
                return appPopup.showResponseAPI(response, status);
            }, function(response, status) {
                return appPopup.showResponseAPI(response, status);
            });
        }
    };
    $scope.users = [{
        id: appAuth.profile.id,
        name: appAuth.profile.name
    }];
    $scope.users = $scope.users.concat(users);
    $scope.coord = plantingsDetail && plantingsDetail.data ? {
        lon: plantingsDetail.data.lon,
        lat: plantingsDetail.data.lat
    } : {};

    // Uploader
    $scope.oldPhotos = $scope.data.photos;
    delete $scope.data.photos;
    $scope.files = [];
    $scope.removeFromQueue = function(index) {
        $scope.files.splice(index, 1);
    };
    $scope.onFileSelect = function(files) {
        files && files.forEach && files.forEach(function(file, key) {
            $scope.files.push({
                id: key,
                file: file
            });
        });
    };
    function doUploadAll(isContinue) {
        Upload.upload({
            url: $scope.data.id ? '/api/v1/plantings/' + $scope.data.id : '/api/v1/plantings',
            method: $scope.data.id ? 'POST' : 'POST',
            data: angular.extend($scope.data, {
                access_token: appAuth.token,
                lon: $scope.coord.lon,
                lat: $scope.coord.lat,
                location_id: $scope.location ? $scope.location.id : $scope.village.id,
                emission_id: $scope.emission.id,
                type: $scope.emission.type == 'Non Fast dan Slow Growing' ? $scope.type : ''
            }, {
                photos: $scope.files.map(function(item) {
                    return item.file;
                }),
                captions: $scope.files.map(function(item) {
                    return item.caption || item.file.name;
                })
            })
        }).then(function(response) {
            if (isContinueAdd) {
                $scope.data = {};
                $scope.coord = {};
                $scope.form.$setPristine();
                $scope.files = [];
            } else {
                if ($state.params.backTo) {
                    $state.go($state.params.backTo)
                } else {
                    $state.go('app.' + $scope.pageInformation.route + '.index')
                }
            }
            return appPopup.showResponseAPI(response, response.status);
        }, function(response) {
            return appPopup.showResponseAPI(response, response.status);
        }, function(evt) {
            $scope.progress = Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
        });
    }
    $scope.clearExt = function(val) {
        return val.replace(/\.([a-zA-Z]{3,})/,'');
    }

    // Location
    $scope.location = $scope.data.location ? $scope.data.location : '';
    $scope.removeLocation = function() {
        $scope.location = null;
    }
    var getLocation = function(query, type, offset, limit) {
        angular.element('.tile').addClass('refreshing');
        var params = {
            access_token: appAuth.token,
            limit: limit || 20,
            offset: offset || 0,
            filter: []
        };
        // Filtering
        if (query) {
            if (angular.isString(query)) {
                params.filter.push({
                    field: 'name',
                    operator: 'like',
                    value: query
                });
            } else if (angular.isObject(query)) {
                for (var key in query) {
                    params.filter.push({
                        field: key,
                        operator: '=',
                        value: query[key]
                    });
                }
            }
        } 
        if (type === 'city') {
            params.filter.push({
                field: 'province_id',
                operator: '=',
                value: '32'
            });
            params.filter.push({
                field: 'city_id',
                operator: '!=',
                value: ''
            });
            params.filter.push({
                field: 'district_id',
                operator: '=',
                value: ''
            });
        } else if (type === 'district') {
            params.filter.push({
                field: 'district_id',
                operator: '!=',
                value: ''
            });  
            params.filter.push({
                field: 'village_id',
                operator: '=',
                value: ''
            });  
        } else {
            params.filter.push({
                field: 'village_id',
                operator: '!=',
                value: ''
            });  
        }
        if (params.filter.length) params.filter = JSON.stringify(params.filter);
        params.order = JSON.stringify([{
            field: 'name',
            sort: 'asc',
        }]);
        return storeLocations.query(params).$promise.then(function(response, status) {
            angular.element('.tile.refreshing').removeClass('refreshing');
            if (response.data) {
                return response.data;
            }
        }, function(response, status) {
            angular.element('.tile.refreshing').removeClass('refreshing');
            appPopup.showResponseAPI(response, status);
        });
    };
    // Kab/Kota
    $scope.city = null;
    $scope.cities = [];
    // Kecamatan
    $scope.district = null;
    $scope.districts = [];
    // Desa
    $scope.village = null;
    $scope.villages = [];
    // Listeners
    getLocation(null, 'city', 0, 1000).then(function(data){
        $scope.cities = data;
    });
    $scope.$watch('city', function(newVal, oldVal) {
        if (newVal) {
            getLocation({ city_id: newVal.city_id }, 'district', 0, 1000).then(function(data){
                $scope.districts = data;
                $scope.district = null;
                $scope.village = null;
            });
        }
    });
    $scope.$watch('district', function(newVal, oldVal) {
        if (newVal) {
            getLocation({ district_id: newVal.district_id }, 'village', 0, 1000).then(function(data){
                $scope.villages = data;
                $scope.village = null;
            });
        }
    });
    
    // Faktor Emisi
    $scope.emissions = emissions;
    $scope.emission = $scope.data.emission_id ? emissions.filter(function(emission){
        return emission.id == $scope.data.emission_id;
    })[0] : null;
    $scope.type = $scope.data.type || null;
	
	$scope.sumberdanaTypes = ['APBD', 'Swakelola', 'Lainnya'];
	
}]);