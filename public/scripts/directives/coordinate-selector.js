'use strict';
/**
 * @ngdoc directive
 * @name minovateApp.directive:vectorMap
 * @description
 * # vectorMap
 */
angular.module('minovateApp').run(['$templateCache', function($templateCache) {
    'use strict';
    $templateCache.put('templates/coordinate-selector.html', '<openlayers ol-center="mapConfig.center" height="200px" ol-defaults="mapConfig.defaults" custom-layers="true">' + '<ol-layer ng-repeat="(key, layer) in mapConfig.layers" ol-layer-properties="layer"></ol-layer>' + '<ol-marker ol-marker-properties="coord"></ol-marker>' + '<div xng-hide="true" style="display:block;z-index:100;position:absolute;bottom:0px;left:0px;right:0;padding: 2px;background:rgba(255,255,255,.5);text-align:center;">' + '<p class="mb-0">Lon: <span>{{ mapConfig.mouseposition.lon | round10:decimal }}</span>, Lat: <span>{{ mapConfig.mouseposition.lat | round10:decimal }}</span></p>' + '</div>' + '</openlayers>');
}]).directive('coordinateSelector', ['$filter', function($filter) {
    return {
        restrict: 'AE',
        templateUrl: 'templates/coordinate-selector.html',
        controller: ['$scope', function($scope) {
            $scope.coord = $scope.coord || {};
            $scope.decimal = angular.isUndefined($scope.decimal) ? -5 : $scope.decimal * -1;
            $scope.mapConfig = {
                center: {
                    lat: $scope.coord.lat || -6.970049417296224,
                    lon: $scope.coord.lon || 107.79235839843751,
                    zoom: 8.5,
                    bounds: [],
                    centerUrlHash: false
                },
                defaults: {
                    view: {
                        maxZoom: 12,
                        minZoom: 3,
                        rotation: 90
                    },
                    layers: {
                        main: {
                            source: {
                                type: 'OSM',
                                url: 'http://{a-c}.tile.opencyclemap.org/cycle/{z}/{x}/{y}.png'
                            }
                        }
                    },
                    interactions: {
                        mouseWheelZoom: true
                    },
                    controls: {
                        zoom: true,
                        rotate: true,
                        attribution: false
                    },
                    controls: {
                        zoom: {
                            position: 'topright'
                        },
                        fullscreen: {
                            position: 'topleft'
                        }
                    },
                    events: {
                        map: ['singleclick', 'pointermove'],
                        // layers: ['mousemove', 'click']
                    }
                },
                layers: {
                    abing: {
                        name: 'Bing',
                        active: true,
                        opacity: 1,
                        source: {
                            type: 'BingMaps',
                            key: 'Aj6XtE1Q1rIvehmjn2Rh1LR2qvMGZ-8vPS9Hn3jCeUiToM77JFnf-kFRzyMELDol',
                            imagerySet: 'Road' // Aerial
                        }
                    },
                    // base_kecamatan: {
                    //     name: 'base_kecamatan',
                    //     label: 'Batas Kecamatan',
                    //     group: 'layers',
                    //     source: {
                    //         type: 'GeoJSON',
                    //         url: 'layers/batas_kec.geojson'
                    //     },
                    //     opacity: 0.5,
                    //     style: {
                    //         fill: {
                    //             color: '#FFFFFF'
                    //         },
                    //         stroke: {
                    //             color: 'black',
                    //             width: 0
                    //         }
                    //     }
                    // }
                },
                selectedLocation: {
                    projection: 'EPSG:4326',
                    lat: -6.970049417296224,
                    lon: 107.79235839843751,
                    label: {
                        message: 'Location',
                        show: true,
                        showOnMouseOver: true
                    }
                },
                mouseposition: {
                    lat: 0,
                    lon: 0
                },
                mouseclickposition: {},
                projection: 'EPSG:4326',
                centerAndShow: function(id) {
                    if (!$scope[id].label.show) {
                        $scope.center.lat = $scope[id].lat;
                        $scope.center.lon = $scope[id].lon;
                    }
                    $scope[id].label.show = !$scope[id].label.show
                }
            };
            $scope.$on('openlayers.map.pointermove', function(event, data) {
                $scope.$apply(function() {
                    if ($scope.mapConfig.projection === data.projection) {
                        $scope.mapConfig.mouseposition = data.coord;
                    } else {
                        var p = ol.proj.transform([data.coord[0], data.coord[1]], data.projection, $scope.mapConfig.projection);
                        $scope.mapConfig.mouseposition = {
                            lat: p[1],
                            lon: p[0],
                            projection: $scope.mapConfig.projection
                        }
                    }
                });
            });
            $scope.$on('openlayers.map.singleclick', function(event, data) {
                if ($scope.readOnly) return;
                $scope.$apply(function() {
                    if ($scope.mapConfig.projection === data.projection) {
                        $scope.coord = data.coord;
                    } else {
                        var p = ol.proj.transform([data.coord[0], data.coord[1]], data.projection, $scope.mapConfig.projection);
                        $scope.coord = {
                            lat: p[1],
                            lon: p[0],
                            projection: $scope.mapConfig.projection
                        };
                    }
                    $scope.coord.lon = $filter('round10')($scope.coord.lon, $scope.decimal);
                    $scope.coord.lat = $filter('round10')($scope.coord.lat, $scope.decimal);
                });
            });
        }],
        scope: {
            coord: '=coordinateSelector',
            readOnly: '=',
            decimal: '@'
        }
        // link: function postLink(scope, element) {
        //   var options = scope.options;
        //   element.vectorMap(options);
        // }
    };
}]);