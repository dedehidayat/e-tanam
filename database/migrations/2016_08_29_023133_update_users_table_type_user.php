<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateUsersTableTypeUser extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->enum('user_type', ['Pengelola', 'Perorangan', 'Badan Hukum'])->nullable();
            $table->text('address');
            $table->string('phone_number');
            $table->string('id_number'); // No. Identitas / Surat Tugas
            // Pengelola Si Biiton
            $table->integer('seedbed_id')->unsigned()->index();
            // $table->foreign('seedbed_id')->references('id')->on('seedbeds')
            //       ->onUpdate('no action')->onDelete('no action')->nullable();
            // Badan Hukum
            $table->string('legal_entity_type');
            $table->string('legal_entity_number');
            $table->string('contact_person');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->drop('user_type');
            $table->drop('address');
            $table->drop('phone_number');
            $table->drop('id_number'); 
            $table->drop('seedbed_id');
            $table->drop('legal_entity_type');
            $table->drop('legal_entity_number');
            $table->drop('contact_person');
        });
    }
}
