<div style="color:#000;background:#f6f6f6;font-family:Verdana,Arial,Helvetica,sans-serif;font-size:12px;margin:0;padding:0"><div class="adM">

</div><div style="color:#000;background:#f6f6f6;font-family:Verdana,Arial,Helvetica,sans-serif;font-size:12px;margin:0;padding:0"><div class="adM">

</div><table cellspacing="0" cellpadding="0" border="0" height="100%" width="100%">

<tbody><tr>

<td align="center" valign="top" style="padding:20px 0 20px 0">

<table bgcolor="FFFFFF" cellspacing="0" cellpadding="10" border="0" width="650" style="border:1px solid #e0e0e0">

<tbody><tr>

<td valign="top">

<h1 style="font-size:22px;font-weight:normal;line-height:22px;margin:0 0 11px 0">Halo, {{ $user->name }}</h1>

<p style="font-size:12px;line-height:16px;margin:0 0 8px 0;margin-top:32px">Kami baru saja menerima permintaan penggantian Kata Sandi akun Anda.</p>

<p style="font-size:12px;line-height:16px;margin:0">
Jika benar Anda mengajukan permintaan ini, klik tautan berikut untuk mengubah Kata Sandi Anda: <a href="{{ $link = $user->getUrlForPaswordReset($token) }}" target="_blank">{{ $link }}</a></p>

<p style="font-size:12px;line-height:16px;margin:0;margin-top:32px">Jika tautan di atas tidak berfungsi, salin dan tempelkan tautan ke browser internet Anda.</p>                     

<p></p>

                                              

</td>

</tr>

<tr>

<td>

</td>

</tr>

</tbody></table>

</td>

</tr>

</tbody></table>

</div>

</div>